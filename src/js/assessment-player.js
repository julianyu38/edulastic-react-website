
/*
================================================================================

  "Assessment Player" page

================================================================================
*/


'use strict';


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *  Toolbar
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

//
// Open/Close question selector
//
document.getElementById('question-selector').addEventListener('click', function() {

  var $selector = this;
  var $dropdown = document.getElementById('question-dropdown');

  if ($selector.classList.contains('js-opened')) {

    $selector.classList.remove('js-opened');
    $dropdown.classList.remove('js-opened');

  } else {

    $selector.classList.add('js-opened');
    $dropdown.classList.add('js-opened');

  }

});
