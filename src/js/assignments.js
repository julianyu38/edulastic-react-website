
/*
================================================================================

  "Assignments" page

================================================================================
*/


'use strict';


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *  "Filter" section
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

//
// Open/Close on mobile
//
document.getElementById('filter-result-total').addEventListener('click', function() {

  var $total = this;
  var $dropdown = document.getElementById('filter-result-dropdown');

  if ($total.classList.contains('js-opened')) {

    $total.classList.remove('js-opened');
    $dropdown.classList.remove('js-opened');

  } else {

    $total.classList.add('js-opened');
    $dropdown.classList.add('js-opened');

  }

});
