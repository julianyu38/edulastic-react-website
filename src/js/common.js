
/*
================================================================================

  Common

================================================================================
*/


'use strict';


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *  Sidebar
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

//
// Collapsing/Expanding
//
document.getElementById('sidebar-arrow-btn').addEventListener('click', function() {

  var $sidebar = document.getElementById('sidebar');
  var $main = document.getElementById('main');
  var $header = document.getElementById('header');

  if ($sidebar.classList.contains('js-collapsed')) {

    $sidebar.classList.remove('js-collapsed');
    $main.classList.remove('js-collapsed');
    $header.classList.remove('js-collapsed');

  } else {

    $sidebar.classList.add('js-collapsed');
    $main.classList.add('js-collapsed');
    $header.classList.add('js-collapsed');

  }

});





/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *  Mobile menu
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

//
// Hamburger button
//
document.getElementById('hamburger').addEventListener('click', function() {

  var $sidebar = document.getElementById('sidebar');

  $sidebar.addEventListener('touchmove', function(e) {
    e.preventDefault();
  });

  $sidebar.classList.add('js-opened');

});


//
// Close button
//
document.getElementById('sidebar-close-btn').addEventListener('click', function() {

  var $sidebar = document.getElementById('sidebar');

  $sidebar.removeEventListener('touchmove', function(e) {
    e.preventDefault();
  });

  $sidebar.classList.remove('js-opened');

});
